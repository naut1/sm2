FROM anibali/pytorch:2.0.0-cuda11.8
#FROM conda/miniconda3
#FROM anibali/pytorch:1.5.0-nocuda
#FROM  pytorch/pytorch:2.0.0-cuda11.7

USER root

WORKDIR /tmp/smp/

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN apt-get update && apt-get install -y  git
#RUN conda update --all -y
RUN pip install tqdm
#RUN conda install torchvision -c pytorch -c nvidia
#RUN conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia

RUN pip install gdown
RUN pip install git+https://github.com/qubvel/segmentation_models.pytorch.git@v0.2.1
RUN pip install -U albumentations[imgaug]


